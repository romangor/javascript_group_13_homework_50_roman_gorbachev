import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() mainTitle = 'My favorite films';
  @Input() subtitle = 'What we do in life echoes in eternity.';
  @Input() bgImg = '';
}
